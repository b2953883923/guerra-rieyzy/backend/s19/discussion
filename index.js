// console.log("Hello Batch 295!");

// [SECTION] Syntax, Statements, and Comments

	// Statements in programming are instructions that we will tell our computer to perform.
		// It has a semicolon to end the.

	// Syntax in programming, it is the set of rules on how the statement must be constructed.

	// Comments are description/documentation of our code/s.


/*
	2 types of comments in JS:
		1. Single Line comments - denoted by two forward slashes (//). Windows: (ctrl + /); Mac: (cmd + /).
		2. Multi-Line comments - denoted by forwards slashes and asterisks. Windows: (ctrl + shift + /); Mac: (cmd + option + /)

*/


// [SECTION] Variables

// Varaibles are used to contain data.


	// Declaring a Variable

	/*
		Syntax:
			let/const variableName;

	*/


	let myVariable;

	console.log(myVariable);


	// Variable declaration with initialization.
	let hello = "Hi!";
	console.log(hello);

	// Variable initialize after declaration
	myVariable = "john doe";
	console.log(myVariable);


	/*
		var vs. let/const

			-let and const
				- cannot be redeclared into the scope.

			-var ES1 (1997)
				- can be re-declared into the scope.
	*/

	// Declaring and Initializing Variables

	let productName = "Desktop Computer";
	console.log(productName);

	let userName = 'John Doe';
	console.log(userName);

	let productPrice = 18999;
	console.log(productPrice);

	const interest = 3.539;
	console.log(interest);

	const pi = 3.1416;
	console.log(pi);

	// Reassiging Variable Values
		// changing the initial or previous value into another value.

		/*
			Syntax:
				variableName = newValue;

		*/

	// productName = "Laptop";
	// console.log(productName);

	productName = "Tablet";
	console.log(productName);


	// This will result to a typeerror
	// pi = 3.1614;
	// console.log(pi);


	// Multiple variable declaration and initialization

	const productCode = "DC017", productBrand = "Dell";
	console.log(productCode, productBrand);

	// Reserved keywords as variable

	// Using reserved keywords will resulat to syntax error.
	// const let = "hello";
	// console.log(let);

	// you can combine reserved keywords with addition word or characters.
	let letVar = "world";
	console.log(letVar);


// [SECTION] Data Types

	// String
	// String are a series characters that create a word, a phrase, a sentence or anything related to text.
	// JS Strings can be writter using either Double Quote ("") or Single Quote ('').

	let country = 'Philippines';
	let province = "Mentro Manila";

	console.log(country, province);


	// Concatenating Strings
	// Multiple string values can be combined to create a single string using the "+" symbol.
	let address = 'I live in ' + province + ', ' + country + '.';
	console.log(address);

	// Escape Character (\) in combinatation with string characters to produce different result.
	// \n - creates a new line.
	let mailAddress = "Metro Manila\nPhilippines";
	console.log(mailAddress);

	let message = "John's employees went home early.";
	console.log(message);

	message = 'John\'s employees went home early.';
	console.log(message);


	// Number Data Types

	// Integer/Whole Numbers
	let headCount = 16;
	console.log(headCount);

	// Decimal Numbers / Fractions
	let grade = 98.7;
	console.log(grade);

	// Exponential Notation
	let planetDistance = 2e10;
	console.log(planetDistance);

	// Combining number with string
	console.log("John's grade last quarter is " + grade);


	// Boolean
	// True or False
	let isMarried = false;
	let isGoodConduct = true;


	console.log("Is John married: " + isMarried);
	console.log("Is John a good boy: " + isGoodConduct);


	// Arrays
	// Arrays are a special kind of data type that is used to store multiple values with the same data type.

		// // Multiple Declaration
		// let grade1 = 98.7, grade2 = 92.1, grade3 =90.5;
		// console.log(grade1, grade2, grade3);

	/*
		Syntax:
			let/const arrayName = [elementA, elementB, elementC ... elementN];

	*/
				  //0    1      2     3
	let grades = [98.7, 92.1, 90.5, 94.6];
	console.log(grades);

	// accessing index value/element
	console.log(grades[0]);

	// Array with different data types
		// it is not recommended.

	let personalDetails = ["John", "Smith", 32, true];
	console.log(personalDetails);

	// Object
	// Objects are another special data type that is used to represent real world objects.

	/*
		Syntax:
			let/const objectName = {
				propertyA: valueA,
				propertyB: valueB
			}

	*/

	
let person = {
		fullName: "Juan Dela Cruz",
		age: 35,
		isMarried: false,
		contactNo: ["+63917-123-5667", "8123-4567"],
		address: {
			houseNumber: "345",
			city: "Manila"
		}
	};

	console.log(person);


	// Select a property of an object
		 // - through dot notation.

	console.log(person.fullName);
	console.log(person.isMarried);
	console.log(person.address.city);
	console.log(person.contactNo[1]);


	// Null and Undefined
	// Null is an assigned value but it ha no meaning.
	// Undefined is a value result from a variable declared without initialization or if the value is out or range.

	let spouse = null;
	console.log(spouse);


	let relationship;
	console.log(relationship);
	console.warn(relationship);












